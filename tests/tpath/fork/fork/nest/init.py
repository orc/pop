import os


def __init__(hub):
    hub.FORK_NEST_INIT = True


def var(hub):
    return hub.FORK_NEST_INIT


def pid(hub):
    return os.getpid()
