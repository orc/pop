# pylint: disable=expression-not-assigned
import os

import pytest

import pop.contract


@pytest.fixture(scope="function")
def hub(hub):
    # Use pytest-pop's hub but scale it down to a function level fixture
    yield hub


@pytest.mark.asyncio
async def test_func_alias(hub):
    hub.pop.sub.add("tests.mods")

    # Verify that a function from a module has been wrapped properly
    assert hub.mods.falias_func.cpu_count() == os.cpu_count()

    # Verify that a lambda function can be wrapped and called correctly
    assert hub.mods.falias_func.λ() == 0

    # Verify that a builtin function function has become an instance of Contracted
    assert isinstance(hub.mods.falias_func.print, pop.contract.Contracted)
    assert hub.mods.falias_func.print.func == print

    # Verify that a builtin function can be wrapped and called without blowing up
    hub.mods.falias_func.print("taco salad", end="f")

    # hub.mods.wrap.already_has_a_hub is an alias for hub.pop.sub.add
    assert "taco" not in hub._subs
    hub.mods.falias_func.already_has_a_hub(dyne_name="taco")
    assert "taco" in hub._subs

    # Verify that an async function can be wrapped
    assert await hub.mods.falias_func.async_func_wrap()


@pytest.mark.asyncio
async def test_func_dict(hub):
    hub.pop.sub.add("tests.mods")

    # Verify that a function from a module has been wrapped properly
    assert hub.mods.falias_dict.cpu_count() == os.cpu_count()

    # Verify that a lambda function can be wrapped and called correctly
    assert hub.mods.falias_dict.λ() == 0

    # Verify that a builtin function function has become an instance of Contracted
    assert isinstance(hub.mods.falias_dict.print, pop.contract.Contracted)
    assert hub.mods.falias_dict.print.func == print

    # Verify that a builtin function can be wrapped and called without blowing up
    hub.mods.falias_dict.print("taco salad", end="f")

    # Verify that an async function can be wrapped
    assert await hub.mods.falias_dict.async_func_wrap()


@pytest.mark.asyncio
async def test_func_alias_docs_example(hub):
    hub.pop.sub.add("tests.mods")
    hub.mods.falias_wrap.do_things_with_our_example()
