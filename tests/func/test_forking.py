import concurrent.futures
import os

import pytest

import pop.hub


@pytest.fixture
def pid():
    yield os.getpid()


@pytest.mark.asyncio
async def test_thread_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add("tests.mods")
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ThreadPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.mods.fork.ping)
    assert ret == {"pid": pid}


@pytest.mark.asyncio
async def test_async_thread_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add("tests.mods")
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ThreadPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.mods.fork.aping)
    assert ret == {"async pid": pid}


@pytest.mark.asyncio
async def test_nested_thread_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add("tests.mods")
    hub.pop.sub.add("tests.mods.nest", sub=hub.mods)
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ThreadPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.mods.nest.basic.pid)
    assert ret == pid


@pytest.mark.asyncio
async def test_process_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add("tests.mods")
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.mods.fork.ping)
    assert ret["pid"] != pid


@pytest.mark.asyncio
async def test_async_process_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add("tests.mods")
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.mods.fork.aping)
    assert ret["async pid"] != pid


@pytest.mark.asyncio
async def test_nested_process_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add("tests.mods")
    hub.pop.sub.add("tests.mods.nest", sub=hub.mods)
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.mods.nest.basic.pid)
    assert ret != pid


@pytest.mark.asyncio
async def test_dyne_thread_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.load_subdirs(hub.fork, recurse=True)
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ThreadPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.nest.init.pid)
    assert ret == pid


@pytest.mark.asyncio
async def test_dyne_process_pool_executor(event_loop, pid):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.load_subdirs(hub.fork, recurse=True)
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.nest.init.pid)
    assert ret != pid


@pytest.mark.asyncio
async def test_dyne_thread_pool_executor_attr(event_loop):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.load_subdirs(hub.fork, recurse=True)
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ThreadPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.nest.init.var)
    assert ret is True


@pytest.mark.asyncio
async def test_dyne_process_pool_executor_attr(event_loop):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.load_subdirs(hub.fork, recurse=True)
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.nest.init.var)
    assert ret is True


def resolver(ref, context):
    return lambda: 42


@pytest.mark.asyncio
async def test_reverse_thread_pool_executor(event_loop):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.dynamic(subname="foo", sub=hub.fork, resolver=resolver)

    # Verify that the dynamic sub was added and is functional
    assert hub.fork.foo.bar() == 42

    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ThreadPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.foo.bar)
    assert ret == 42


@pytest.mark.asyncio
async def test_reverse_process_pool_executor(event_loop):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.dynamic(subname="foo", sub=hub.fork, resolver=resolver)

    # Verify that the dynamic sub was added and is functional
    assert hub.fork.foo.bar() == 42

    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.foo.bar)
    assert ret == 42


@pytest.mark.asyncio
async def test_proxy_reverse_thread_pool_executor(event_loop):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.dynamic(subname="foo", sub=hub.fork, resolver=resolver)

    # Verify that the dynamic sub was added and is functional
    assert hub.fork.rsub.caller() == 42

    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ThreadPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.rsub.caller)
    assert ret == 42


@pytest.mark.asyncio
async def test_proxy_reverse_process_pool_executor(event_loop):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="fork")
    hub.pop.sub.dynamic(subname="foo", sub=hub.fork, resolver=resolver)

    # Verify that the dynamic sub was added and is functional
    assert hub.fork.rsub.caller() == 42

    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(hub.fork.rsub.caller)
    assert ret == 42


@pytest.mark.asyncio
async def test_non_hub_function_wrap(event_loop, pid):
    hub = pop.hub.Hub()

    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()
    ret = await hub.pop.loop.wrap(os.getpid)
    assert ret != pid


def test_example():
    # Test the example from the release docs as it is written in the docs
    hub = pop.hub.Hub()
    hub.pop.loop.create()
    hub.pop.loop.EXECUTOR = concurrent.futures.ProcessPoolExecutor()

    ret = hub.pop.loop.wrap(os.getpid)

    forked_pid = hub.pop.Loop.run_until_complete(ret)

    pid = os.getpid()
    assert forked_pid != pid
