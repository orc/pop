import pop.hub


def test_when_dyne_sub_is_added_with_recursive_contracts_the_contracts_should_be_applied(
    capsys,
):
    hub = pop.hub.Hub()
    hub.pop.sub.add(
        pypath="tests.integration.recursive_contract.rc_sub", subname="test_sub"
    )

    expected_result = [
        "regular-pre",
        "recursive-pre",
        "regular-pre-call",
        "actual-call",
        "regular-post-call",
        "regular-post",
        "recursive-post",
    ]
    expected_output = "regular-pre\nrecursive-pre\nregular-pre-call\nactual-call\nregular-post-call\nregular-post\nrecursive-post\n"
    hub.pop.sub.load_subdirs(hub.test_sub, recurse=True)

    actual_result = hub.test_sub.nested_sub.fnord.test_call()

    output = capsys.readouterr()
    assert output.out == expected_output
    assert actual_result == expected_result
