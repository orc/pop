def pre(hub, ctx):
    print("recursive-pre")
    ctx.extra = (getattr(ctx, "extra", None) or []) + ["recursive-pre"]


def post(hub, ctx):
    print("recursive-post")
    ctx.ret.append("recursive-post")


def call(hub, ctx):
    print("recursive-pre-call")
    result = ctx.extra + [
        "recursive-pre-call",
        ctx.func(hub=hub),
        "recursive-post-call",
    ]
    print("recursive-post-call")
    return result
