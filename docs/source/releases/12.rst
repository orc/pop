==============
POP 12 Release
==============

This release cleans up a few internal refs and adds features to the unit
testing framework.

The main change that may create issues for other projects is that the
internal name value for plugins was changed from ``__sub_name__`` to
`__name__`. This is because in the original implementation plugins
were referred to as "submodules" and the term ``sub`` subsequently
became greatly overloaded. This, as well as other fixes, makes the
term ``sub`` ONLY reference plugin subsystems.
